# desafio-broker

<div style="text-align:center" align="center"><img src="https://gitlab.com/desafio-banco-inter-thales/solution/-/raw/master/public/tenor.gif" alt="money"/></div>

### Esse desafio foi realizado seguindo as regras estabelecidas pelo documento [PDF](https://drive.google.com/file/d/1AcscPlXtFSEpvHK3IN6_QR9ylMO4ZCwG/view?usp=sharing)


</br>

### Antes da implementação do projeto houve uma concepção sobre a ideia completamente [documentada aqui](https://heady-swift-7de.notion.site/Desafio-inter-515d4b464bfc44cdb60e882dab1159b3)

</br>

### Alguns diagramas foram criados com auxílio do diagrams.net e se encontram [disponíveis aqui](https://app.diagrams.net/#Adesafio-banco-inter-thales%2Fsolution%2Fmaster%2Fpublic%2FUntitled%20Diagram.drawio)


<br/>

O projeto é composto por três microserviços e ao final do fluxo demonstrado abaixo será possível obter o exemplo de resposta esperado mais a frente:

<div style="text-align:center" align="center"><img src="https://gitlab.com/desafio-banco-inter-thales/solution/-/raw/master/public/desafio-Relacionamento.drawio.png" alt="relacionamento"/></div>

<br/>

[User manager repo](https://gitlab.com/desafio-banco-inter-thales/user-manager)
<br/>
[Stock manager repo](https://gitlab.com/desafio-banco-inter-thales/stock-manager)
<br/>
[Company manager repo](https://gitlab.com/desafio-banco-inter-thales/company-manager)

<hr/>

## Atualização 20/08/2022

> Existem novas branchs que melhoram a arquitetura do projeto adicionando uma API Gateway,
> monitoramento e mais. [Clique aqui para entender mudanças e futuras atualizações do projeto](https://gitlab.com/desafio-banco-inter-thales/solution/-/blob/master/README20082022.md)

## Fim da Atualização de 20/08/2022

### Como rodar o projeto

1. Clonando o repositório
  - Entre na pasta deste projeto depois de clonado
  - Certifique-se de ter o `docker` e o `docker-compose` instalados em sua máquina
  - Execute `docker-compose up -d`

### Como executar um teste simples

1. Abra seu navegador preferido
2. Você precisará de três abas referentes a cada serviço
3. User manager -> http://localhost:8080/swagger-ui/index.html
    - Crie pelo menos um usuário administrador
4. Company manager -> http://localhost:8081/swagger-ui/index.html
    - Gerencie as companhias do projeto a partir de qualquer usuário administrador
5. Stock manager -> http://localhost:8082/swagger-ui/index.html
    - Tendo criado ao menos um usuário e uma empresa execute o endpoint de criação de stocks

### Exemplo de Resposta

Um exemplo de requisição será demonstrado abaixo

POST /companies/[ticker-da-empresa]
```
{
  "userCpf": "",
  "order": 600,
  "amountStocksYouWant": 1
}
```

Um exemplo da resposta será demonstrado abaixo


```
{
  "exchange": 7.24,
  "total": 592.76,
  "averagePrice": 592.76,
  "stocks": [
    {
      "ticker": "BIDI11",
      "amount": 58,
      "total": 592.76,
      "historyAmountXPrice": [
        {
          "58": 10.22
        }
      ]
    }
  ]
}
```

### Características do projeto

- Foi criado utilizando Spring como framework **Java**;
  - O motivo da escolha pelo Spring ser uma ferramenta familiar ao autor.
- Não foi implementado o nível `HATEOAS` já que existe outra ferramenta que facilita a navegação do usuário pela aplicação.
- Java **v17.0.3-zulu** utilizando o **sdkman**
- Foram utilizadas as seguintes libs(A motivação de cada uma pode estar bem mais detalhada no [documento](https://heady-swift-7de.notion.site/Desafio-inter-515d4b464bfc44cdb60e882dab1159b3) mencionado anteriormente):
    - [x] JUnit -> Facilita o desenvolvimento e execução de testes unitários;
    - [x] Mockito -> Auxilia na criação de mocks para os testes unitários;
    - [x] H2 -> Como uma constraint estabelecida pelo desafio o H2 foi escolhido e explicado pelo documento mencionado anteriormente;
    - [x] Swagger utilizando a Open API spec -> auxilia a descrição, consumo e visualização de serviços de uma API REST;
    - [x] Undertow -> Servidor escolhido por ser bem mais performático que o tomcat;
    - [x] spring-data-jpa -> Facilitar o acesso a camada de persistência;
    - [x] spring-cloud-stream -> ferramenta para facilitar a construção de EDA;
    - [x] kafka -> Para comunicação entre os microserviços;
    - [x] sleuth -> Explicação detalhada a frente;
    - [x] zipkin -> Explicação detalhada a frente.

- Utiliza o gitlab-ci para;
    - Realização de `testes automatizados`;
    - Realização de analise do código com `sonarcloud` com cobertura > 65%;
- Utiliza os conceitos do `Git Flow`;
- O `K8s` não foi utilizado nesse projeto, em razão do autor não julgar necessária a implantação em produção desse projeto;
- O `docker` foi utilizado o que facilita bastante a implantação dos artefatos nos grandes vendors que o autor julgar interessante no futuro

## Sleuth e Zipkin

Como arquitetura baseada em microserviços conseguimos resolver alguns problemas
que poderiam ser encontrados relativo a utilizar uma arquitetura em monolito.<br/><br/>

Quando pensamos em escalabilidade poderemos escalar nosso sistema sem precisar
alocar recursos adicionais para apps que não estão sendo tão utilizadas.<br/><br/>

Mas da mesma forma que resolvemos problemas de um lado, criamos de outro e
com isso vem a replicação de código. Pensando em centralizarmos esses logs
distribuídos entre nossos microserviços é necessário agrupar os logs de uma
app em comparação a outra.<br/><br/>

Daí surge a necessidade de utilização do Sleuth e para facilitar a visualização
do tracing o autor decidiu utilizar o zipkin.<br/><br/>

A cada log gerado pela aplicação perceba que geramos 4 informações dentro de
colchetes.

Ex: **[user-manager,9a58bba4efb0965e,9a58bba4efb0965e]**

- O primeiro parâmetro é nome da aplicação que gerou o log;
- O Segundo parâmetro é o trace id, no caso da imagem acima todos são iguais porque estão pertecem ao mesmo contexto;
- O terceiro parâmetro é o span id, cada requisição nas aplicações gera um novo id;
- O quarto parâmetro indica se o log será sincronizado para alguma ferramenta, que dependendo da configuração nem todos os logs serão enviados.

> Depois de utilizar o Swagger para controlar a aplicação, sugiro que entre na porta
> http://localhost:9411/ para visualização dos logs de forma separada.
> 
### Premissas
- [x] Subir o código em um repositório público para que seja avaliado;
    - Gitlab
- [x] Você deve utilizar um dos bancos: H2, HSQL, SQLite ou Derby;
    - H2
- [x] Utilizar um dos frameworks: Spring boot, Micronaut ou Quark;
    - Spring boot
- [x] Aplicar o máximo de metodologias conhecidas, como cache, resiliência etc.;
    - [documentação](https://heady-swift-7de.notion.site/Desafio-inter-515d4b464bfc44cdb60e882dab1159b3)
- [x] Testes unitários são obrigatórios;
    - Foram implementados testes unitários e de integração
- [x] Documentar os serviços e o contrato de cada endpoint com ferramentas conhecidas, como Swagger, OpenApi etc.;
    - Swagger
- [x] Criar um readme explicando como rodar a aplicação;

### Atividades

> Utilizando o endpoint referente ao [company-manager](http://localhost:8081/swagger-ui/index.html#/)

- [x] Cadastrar as empresas com os preços das ações;
- [x] Atualizar o preço de uma ação via sistema;
- [x] Atualizar o status de uma empresa para Ativa e/ou Inativa;
- [x] Excluir a empresa cadastrada;
- [x] consultar a lista de empresas cadastradas, por status (Ativa/Inativa) e/ou por todas que foram incluídas;

> Utilizando o endpoint referente ao [stock-manager](http://localhost:8082/swagger-ui/index.html#/)

- [x] Consultar a lista de investimentos realizados,;
- [x] Criar investimentos dado uma ordem;   
    - É necessário que o sistema distribua o dinheiro da forma mais homogênea e eficiente possível entre as empresas com status Ativa, de modo a dar o menor troco possível para o usuário.
